const express = require("express");
const app = express();
const hbs = require ("hbs");
const router = require('./routes/public');
const routerAdmin = require('./routes/admin');
const bodyParser = require("body-parser");
const routerAuth = require('./routes/auth');
const session = require("express-session");
require ("dotenv").config();


app.use(session({
    secret: 'your-secret-key',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false } 
}));

app.use((req, res, next) => {
    res.locals.isAuthenticated = !!req.session.userId;
    next();
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));
//app.use ("/", router);

app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + "/views/partials");

app.use(express.static(__dirname + "/public"));
app.set('views', __dirname + "/views");

app.use("/", router);
app.use("/admin", routerAdmin);
app.use("/auth", routerAuth);

const adminController = {
    index: (req, res) => {
        try {
            // Establecer la sesión del usuario
            req.session.nombre = "Nicolas";
            req.session.logueado = false;
            res.render("admin/index");
        } catch (error) {
            console.error("Error en el controlador de inicio:", error);
            res.status(500).send("Error interno del servidor");
        }
    }
};

const puerto = process.env.PORT || 3000;

app.listen(puerto, () => {
    console.log(`Servidor coriendo en el puerto ${puerto}`);
    console.log(`http://localhost:${puerto}/`);
});

module.exports = adminController;
    
