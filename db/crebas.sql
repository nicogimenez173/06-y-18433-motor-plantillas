DROP TABLE IF EXISTS "integrantes";
DROP TABLE IF EXISTS "media";
DROP TABLE IF EXISTS "media2";
DROP TABLE IF EXISTS "usuarios";

CREATE TABLE IF NOT EXISTS "integrantes" (
	"id" INTEGER NOT NULL UNIQUE,
	"matricula" TEXT,
	"nombre" TEXT,
	"apellido" TEXT,
	"estado" BOOLEAN NOT NULL,
	PRIMARY KEY("id")	
);

CREATE TABLE IF NOT EXISTS "media" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT,
	"estado" BOOLEAN NOT NULL,
	PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS "media2" (
	"id" INTEGER NOT NULL UNIQUE,
	"Tmedia" INTEGER,
	"Titulo" TEXT,
	"src" TEXT,
	"url" TEXT,
	"matricula" TEXT,	
	"integrante" TEXT,
	"estado" BOOLEAN NOT NULL,
	PRIMARY KEY("id"),
	FOREIGN KEY ("Tmedia") REFERENCES "media"("id")
	ON UPDATE NO ACTION ON DELETE NO ACTION,
	FOREIGN KEY ("integrante") REFERENCES "integrantes"("nombre")
	ON UPDATE NO ACTION ON DELETE NO ACTION
	FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
	ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS "usuarios" (
	"id" INTEGER NOT NULL UNIQUE,
	"email" TEXT NOT NULL,
	"password" TEXT NOT NULL,
	"superadmin" INTEGER NOT NULL,
	"matricula" INTEGER,
	PRIMARY KEY("id"),
	FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
	ON UPDATE NO ACTION ON DELETE NO ACTION

);
