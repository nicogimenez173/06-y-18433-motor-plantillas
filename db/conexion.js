const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, 'integrantes.sqlite');
//const db = new sqlite3.Database(dbPath);
//const { open } = require('sqlite');

const db= new sqlite3.Database("./db/integrantes.sqlite",
    sqlite3.OPEN_READWRITE,
    (error) => {
        if(error)
            console.log("ocurrio un error", error.message);
        else{
            console.log("conexion exitosa");
            db.run("select * from integrantes");
        }
    }
);

async function getAll(query, params){
    return new Promise ((resolve, reject) => {
        db.all(query, params, (error, rows) =>{
            if (error) return reject (error);
            resolve (rows);
        });
    });
}

async function getOne(query, params = []) {
    return new Promise((resolve, reject) => {
        db.get(query, params, (error, row) => {
            if (error) {
                reject(error);
            } else {
                resolve(row);
            }
        });
    });
}

async function asociacion (id) {
    const result = await getAll("SELECT 1 FROM media2 WHERE matricula = ? AND estado = 1", [id]);
    return result.length > 0;
}

async function asociacionTipoMedia (id) {
    const result = await getAll("SELECT 1 FROM media2 WHERE Tmedia = ? AND estado = 1", [id]);
    return result.length > 0;
}



module.exports = {db, getAll, getOne, asociacionTipoMedia, asociacion};