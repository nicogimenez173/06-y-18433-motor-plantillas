INSERT INTO integrantes (matricula, nombre, apellido, estado) VALUES
    ('Y18433','Nicolas', 'Gimenez', 1),
    ('Y26230','Zuanny', 'Ortiz', 1),
    ('Y23715','Gabriela', 'Espinola', 1),
    ('Y12954','Yenifer', 'Aguilera',1),
    ('UG0045','Diego', 'Ramirez', 1);

INSERT INTO media (nombre, estado) VALUES
('Youtube', 1),
('imagen', 1),
('dibujo', 1);

INSERT INTO media2 (integrante,matricula, Tmedia, titulo, url, src, estado) VALUES
('Nicolas','Y18433', 1, 'Video de Youtube','https://www.youtube.com/embed/YZ8j6iO0ulw?si=3qmb7GQ0CxHN2C3Q', NULL, 1 ),
('Nicolas','Y18433', 2, 'Imagen elegida', NULL, '/Images/mis_sueños.jpeg', 1 ),
('Nicolas','Y18433', 3, 'Dibujo hecho', NULL, '/Images/DibujoNico.png', 1),

('Zuanny','Y26230', 1, 'Video de Youtube','https://www.youtube.com/embed/Qes1RMK9a50?si=U5YTt8otRVQc1Ahk', NULL, 1 ),
('Zuanny','Y26230', 2, 'Imagen elegida', NULL,'/Images/Extrovertida.jpg' , 1 ),
('Zuanny','Y26230', 3, 'Dibujo hecho', NULL, '/Images/Dibujo_Zuanny.png', 1),

('Gabriela','Y23715', 1, 'Video de Youtube','https://www.youtube.com/embed/YehKucEI-Gc', NULL, 1 ),
('Gabriela','Y23715', 2, 'Imagen elegida', NULL, '/Images/Bob%20Esponja.jpeg', 1 ),
('Gabriela','Y23715', 3, 'Dibujo hecho', NULL, '/Images/bananamichi.png', 1),

('Yenifer','Y12954', 1, 'Video de Youtube','https://www.youtube.com/embed/yKNxeF4KMsY?si=_ipSNyBEBlw03PPN', NULL, 1 ),
('Yenifer','Y12954', 2, 'Imagen elegida', NULL, '/images/Yenniimagen.JPEG', 1 ),
('Yenifer','Y12954', 3, 'Dibujo hecho', NULL, '/Images/Yennidibujo.JPEG', 1),

('Diego','UG0045', 1, 'Video de Youtube','https://www.youtube.com/embed/yKNxeF4KMsY?si=_ipSNyBEBlw03PPN', NULL, 1 ),
('Diego','UG0045', 2, 'Imagen elegida', NULL,'/Images/imagen1Diego.jpeg' , 1 ),
('Diego','UG0045', 3, 'Dibujo hecho', NULL, '/Images/Imagen2Diego.jpeg', 1);

INSERT INTO usuarios (email, password, superadmin, matricula)
VALUES ('Prueba@gmail.com', '$2b$10$fdW7kJ0QTKWxuIJYie4YweHjeHJ2GkXSs7JA.bLhK/oz2mynY9cKu', 1, 'Y18433')



