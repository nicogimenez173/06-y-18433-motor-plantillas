const Joi = require("joi");

const editIntegranteStoreSchema = Joi.object({

    matricula: Joi.string()
            .required()
            .messages({
                "string.empty": "La matricula no puede estar vacía",
                "any.required": "La matricula es requerida",
            }),    
    nombre: Joi.string()
            .required()
            .messages({
                "string.empty": "El nombre no puede estar vacío",
                "any.required": "El nombre es requerido",
            }),

        apellido: Joi.string()
            .required()
            .messages({
                "string.empty": "El apellido no puede estar vacío",
                "any.required": "El apellido es requerido",
            }),

        
        
    }).options({abortEarly: false});

module.exports = editIntegranteStoreSchema;
