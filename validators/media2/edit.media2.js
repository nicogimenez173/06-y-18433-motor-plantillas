const Joi = require("joi");

const media2editSchema = Joi.object({
    Tmedia: Joi.string()
        .required()
        .messages({
            "string.empty": "El campo tipo media no puede estar vacío",
            "any.required": "El campo tipo media es requerido",
        }),
    titulo: Joi.string()
        .required()
        .messages({
            "string.empty": "El titulo no puede estar vacío",
            "any.required": "El titulo es requerido",
        }),
    
    srcPath: Joi.binary().optional().messages({
        "binary.base": "El SRC debe ser un archivo válido",
    }),

    url: Joi.string().uri().optional().messages({
        "string.url": "La URL debe ser válida",
    }),
    
    matricula: Joi.string()
        .required()
        .messages({
            "string.empty": "El campo matricula no puede estar vacía",
            "any.required": "El campo matricula es requerida",
        }),
    
    
    integrante: Joi.string()
        .required()
        .messages({
            "string.empty": "El campo integrante no puede estar vacía",
            "any.required": "El campo integrante es requerida",
        }),
        
    
    
    

}).options({abortEarly: false});

module.exports = media2editSchema;
