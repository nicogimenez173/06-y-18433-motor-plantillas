const Joi = require("joi");

const mediaEditSchema = Joi.object({
    nombre: Joi.string()
        .required()
        .messages({
            "string.empty": "El nombre no puede estar vacío",
            "any.required": "El nombre es requerido",
        }),

});

module.exports = mediaEditSchema;
