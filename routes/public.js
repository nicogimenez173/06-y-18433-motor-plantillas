require ("dotenv").config();

const express = require ("express");
const router = express.Router();


const {getAll} = require ("../db/conexion");



router.get("/", async (req, res) => {
    const integrantes = await getAll("select * from integrantes where estado = 1");
   // console.log("INTEGRANTES", integrantes);
    console.log("INTE");
    res.render("index", {
        integrantes:integrantes,
    materia:process.env.db_materia,
    alumno:process.env.db_nombre,
    apellido:process.env.db_apellido,
    repositorio:process.env.db_enlace
    });
    
});


//dbSqlite.all("select * from integrantes", (err, rows) => {
  //  console.log(err, rows);
    //if(err) response.status(500);
    //else return rows;})

//router.get("/", async (request, response)=> {
    

    //const rows = dbSqlite.all("select * from integrantes", (err, rows) => {
        //console.log(err, rows);
        //if(err) response.status(500); //error de servidor
        //else return rows;
    
    //});
//});
//const matriculas = [...new Set(db.media2.map(item => item.matricula))];
router.get("/:matricula", async (request, response) => {
    const integrantes = await getAll("select * from integrantes where estado = 1");
    //console.log("INTEGRANTES", integrantes);

    const matricula = request.params.matricula;
    //console.log(matricula)
    //if (matriculas.includes(matricula)) {
        const integrantesFilter=await getAll ("select * from integrantes where matricula = ?", [matricula]);
        //const integrantesList=await getAll ("select * from integrantes",);
        const media2Filter=await getAll ("select * from media2 where matricula = ?", [matricula]);
        response.render('conjunto', {
            materia:process.env.db_materia,
            alumno:process.env.db_nombre,
            apellido:process.env.db_apellido,
            repositorio:process.env.db_enlace,
            media2: media2Filter,
            integrante: integrantesFilter,
            integrantes: integrantes
            
        });
    //}else{
      //app.use((req, res, next) => {
        //res.status(404).render('partials/error');
    //});  
    //}
    
});
//console.log ("base de datos simulada", db);
//console.log (db.integrantes);

module.exports = router;

