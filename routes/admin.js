const express = require ("express");
const router = express.Router();
const fs = require('fs');
const multer = require('multer');
const upload = multer({ dest: './public/images' })
const fileUpload = upload.single('src');
const AdminIntegrantesController = require("../controllers/admin/intengrantes.controller");
const AdminMediaController = require("../controllers/admin/media.controller");
const AdminMedia2Controller = require("../controllers/admin/media2.controller");

router.get("/main", async (req, res) => {
    console.log("probando")
    res.render("admin/index", {   
    });
});

//integrantes
router.get("/integrantes/listar", AdminIntegrantesController.index);
router.get("/integrantes/crear",AdminIntegrantesController.create);
router.post('/integrantes/create', AdminIntegrantesController.store);
router.get('/integrantes/edit/:idintegrante', AdminIntegrantesController.edit);
router.post('/integrantes/update/:idintegrante', AdminIntegrantesController.update);
router.post('/integrantes/delete/:idintegrante', AdminIntegrantesController.destroy);

//tipo de media
router.get('/media/listar', AdminMediaController.index);
router.get("/media/crear", AdminMediaController.create);
router.post('/media/create', AdminMediaController.store);
router.get('/media/edit/:idmedia', AdminMediaController.edit);
router.post('/media/update/:idmedia', AdminMediaController.update);
router.post('/media/delete/:idmedia', AdminMediaController.destroy);

//medios
router.get('/media2/listar', AdminMedia2Controller.index);
router.get("/media2/crear", AdminMedia2Controller.create);
router.post('/media2/create', fileUpload, AdminMedia2Controller.store);
router.get('/media2/edit/:idmedia2', AdminMedia2Controller.edit);
router.post('/media2/update/:idmedia2',fileUpload, AdminMedia2Controller.update);
router.post('/media2/delete/:idmedia2', AdminMedia2Controller.destroy);


module.exports = router;