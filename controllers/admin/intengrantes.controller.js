const integranteStoreSchema = require("../../validators/integrantes/create.integrantes");
const editIntegranteStoreSchema = require("../../validators/integrantes/edit.integrantes");
const modelIntegrante = require("../../models/model.integrante");

/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/


const IntegrantesController = {
    index: async function (req, res) {
        try {
          console.log("filtrado", req.query);
    let integrantes = await modelIntegrante.getAll (req, res) ;
        res.render("admin/integrantes/index", {  
            integrantes:integrantes, 
        });  
        }catch (error) {
            console.error('Error al obtener los integrantes:', error);
            res.status(500).send('Error al obtener los integrantes');
        }        
    },


    create: async function (req, res) {
        res.render('admin/integrantes/crearForm');
    },


    store: async function (req, res) {
        const {error, value} = integranteStoreSchema.validate(req.body);
        if (error) {
            const mensaje = error.details.map (detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error:mensajeFormateado});
        }else{
          try {
            const {matricula, nombre, apellido, estado} = req.body;
            const nuevoReq = {matricula, nombre, apellido, estado};
            const existeIntegrante = await modelIntegrante.getByField('integrantes', 'matricula', matricula);
            if (existeIntegrante) {
                return res.status(400).json({ error: '¡Ya existe un integrante con la matrícula que has introducido!' });
            }
            await modelIntegrante.create(nuevoReq);
            res.json({ success: true,message: 'Registro creado correctamente.' });
        } catch (error) {
            console.error('Error al crear el integrante:', error);
            res.status(500).json({ error: 'Error al crear el integrante' });
        }

        }

        
    

    },

    show: async function (req, res) {

    },

    update: async function (req, res) {
        console.log ("req.body", req.body);  
    
       const idintegrante = parseInt(req.params.idintegrante);
        console.log("idintegrante", idintegrante);
                
        const {error, value} = editIntegranteStoreSchema.validate(req.body);

        if (error) {
            const mensaje = error.details.map (detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error:mensajeFormateado});
        }else {
        try {
            await modelIntegrante.update(req.body, idintegrante);
                res.json({success: true, message: 'Registro actualizado correctamente.'});
            } catch (error) {
                console.error('Error al actualizar el integrante:', error);
                res.status(500).json({error: '¡Error al actualizar al integrante!'});
            }
        }


    },

    edit: async function (req, res) {
        const idIntegrante = parseInt(req.params.idintegrante);
        modelIntegrante.getById(idIntegrante)
            .catch((err) => {
                console.error('Error al obtener el integrante:', error);
                res.status(500).send('Error al obtener el integrante');
            })
            .then((integrantes) => {
                if (!integrantes) {
                    console.error('Error Integrante no encontrado');
                    res.status(404).send('Integrante no encontrado');
                } else {
                    res.render('admin/integrantes/editForm', {
                        integrantes: integrantes
                    });
                }
            })


    },

    destroy: async function (req, res) {
        const idIntegrante = parseInt(req.params.idintegrante);
        
            try {
                await modelIntegrante.delete(idIntegrante);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (error) {
                console.error('Error al eliminar el integrante:', error);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        
    },


};



module.exports = IntegrantesController;