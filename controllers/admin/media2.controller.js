const express = require ("express");
const router = express.Router();
const {db, getAll, getOne} = require('../../db/conexion');
const fs = require("fs");
//const sqlite3 = require('sqlite3');
//const path = require('path');
const media2StoreSchema = require("../../validators/media2/create.media2");
const media2editSchema = require("../../validators/media2/edit.media2");
const modelmedia2 = require("../../models/model.media2");

const media2controller = {
    index: async function (req, res) {
        try {
          console.log("filtrado", req.query);
    let media2 = await modelmedia2.getAll (req, res) ;
        res.render("admin/media2/index", {  
            media2:media2, 
        });  
        }catch (error) {
            console.error('Error al obtener el media:', error);
            res.status(500).send('Error al obtener los  medios');
        }
    },

    create: async function (req, res) {
        res.render('admin/media2/crearForm');
    },

    store: async function (req, res) {
        console.log ("req.body", req);      
        const {error, value} = media2StoreSchema.validate(req.body);
        let src = "";
        const errors = [];


        if (error) {
            const mensaje = error.details.map (detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error:mensajeFormateado});
        }else{
            
            if (req.file) {
                const destino = `public/Images/${req.file.originalname}`;
                try {
                    await fs.rename(req.file.path, destino);
                    src = `/images/${req.file.originalname}`;
                } catch (err) {
                    console.error(err);
                    errors.push('¡Error al subir la imagen!');
                }
            }
            try {
                const { Tmedia, Titulo, url, matricula, integrante, estado} = req.body;
                const nuevoReq = { Tmedia, Titulo, src, url, matricula, integrante, estado };

                await modelmedia2.create(nuevoReq);
                res.redirect(`/admin/media2/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.error(err);
                res.redirect(`/admin/media2/create?errors=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
        if (errors.length > 0) {
            return res.redirect(`/admin/media2/create?errors=${encodeURIComponent(JSON.stringify(errors))}`);
        }

        },  
            /*try{
                
              ,
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/media2/listar");
                }
            
           
               
        }*/
         
        


        

        
   

    show: async function (req, res) {},

    update: async function (req, res) {
        console.log ("req.body", req.body);
        const idmedia2 = parseInt(req.params.idmedia2);
        console.log("idmedia2", idmedia2);
        const mediaExiste = await getOne("SELECT * FROM media2 WHERE id = ?", [idmedia2]);
        const {error, value} = media2editSchema.validate(req.body);
        if (error) {
            const mensaje = error.details.map (detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error:mensajeFormateado});
        }else {
            const mediaExistente = await getOne("SELECT * FROM media WHERE id = ?", [id]);
            try {
                let src = '';
                if (mediaExistente.src) {
                    src = mediaExistente.src;
                }
                if (req.file) {
                    var tpm_path = req.file.path;
                    var destino = "public/images/" + req.file.originalname;
                    try {
                        await fs.rename(tpm_path, destino);
                        src = "/images/" + req.file.originalname;
                    } catch (err) {
                        return res.sendStatus(500);
                    }
                }
                await mediaModel.update(req.body, id, src); // Pasa src como parámetro
                res.json({success: true, message: 'Registro actualizado correctamente.'});
            } catch (error) {
                console.error('Error al actualizar el integrante:', error);
                res.status(500).json({error: '¡Error al actualizar al integrante!'});
            }
        }
    },


    

    edit: async function (req, res) {
        const idmedia2 = parseInt(req.params.idmedia2);
        modelmedia2.getById(idmedia2)
            .catch((err) => {
                console.error('Error al obtener el media:', error);
                res.status(500).send('Error al obtener el media');
            })
            .then((media2) => {
                if (!media2) {
                    console.error('Error media no encontrado');
                    res.status(404).send('media no encontrado');
                } else {
                    res.render('admin/media2/editForm', {
                        media2: media2
                    });
                }
            })


    },
    
    destroy: async function (req, res) {
        const idmedia2 = parseInt(req.params.idmedia);
        
            try {
                await modelmedia2.delete(idmedia2);
                res.redirect("/admin/media2/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (error) {
                console.error('Error al eliminar el media:', error);
                res.redirect("/admin/media2/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
    },

};

module.exports = media2controller;