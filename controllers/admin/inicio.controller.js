const {request} = require("express-session");

const inicioController = {
    index: (req, res) => {
        req.session.nombre = "Nicolas";
        req.session.logueado = false;
        req.session.userInfo = {
            nombre: "Nicolas",
            logueado: false,
        };


        res.render("admin/index");
    }
}

module.exports = inicioController;
