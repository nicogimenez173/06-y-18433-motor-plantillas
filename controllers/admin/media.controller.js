const mediaStoreSchema = require("../../validators/media/create.media");
const mediaEditStoreSchema = require("../../validators/media/edit.media");
const modelmedia = require("../../models/model.media");


const mediacontroller = {
    index: async function (req, res) {
        try {
          console.log("filtrado", req.query);
    let media = await modelmedia.getAll (req, res) ;
        res.render("admin/media/index", {  
            media:media, 
        });  
        }catch (error) {
            console.error('Error al obtener los tipos media:', error);
            res.status(500).send('Error al obtener los tipos media');
        }
    },

    create: async function (req, res) {
        res.render('admin/media/crearForm');
    },

    store: async function (req, res) {
        const {error, value} = mediaStoreSchema.validate(req.body);
        if (error) {
            const mensaje = error.details.map (detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error:mensajeFormateado});
        }else{
          try {
            const {nombre, estado} = req.body;
            const nuevoReq = {nombre, estado};
            const existemedia = await modelmedia.getByField('media', 'nombre', nombre);
            if (existemedia) {
                return res.status(400).json({ error: '¡Ya existe este tipo de media' });
            }
            await modelmedia.create(nuevoReq);
            res.json({ success: true,message: 'Registro creado correctamente.' });
        } catch (error) {
            console.error('Error al crear el tipo de media:', error);
            res.status(500).json({ error: 'Error al crear el tipo de media' });
        }

        }    
    },

    //show: async function (req, res) {},

    update: async function (req, res) {
        console.log ("req.body", req.body); 
        const idmedia = parseInt(req.params.idmedia);
        console.log ("idmedia", idmedia);
        const {error, value} = mediaEditStoreSchema.validate(req.body);
        if (error) {
            const mensaje = error.details.map (detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error:mensajeFormateado});
        }else{
            try{
                await modelmedia.update(req.body, idmedia);
                res.json({success: true, message: 'Registro actualizado correctamente.'});
            } catch (error) {
                console.error('Error al actualizar el media:', error);
                res.status(500).json({error: '¡Error al actualizar tipo de media media!'});
            }
        }



    },

    edit: async function (req, res) {
        const idmedia = parseInt(req.params.idmedia);
        modelmedia.getById(idmedia)
            .catch((err) => {
                console.error('Error al obtener el media:', error);
                res.status(500).send('Error al obtener el media');
            })
            .then((media) => {
                if (!media) {
                    console.error('Error media no encontrado');
                    res.status(404).send('media no encontrado');
                } else {
                    res.render('admin/media/editForm', {
                        media: media
                    });
                }
            })
    },

    destroy: async function (req, res) {
        const idmedia = parseInt(req.params.idmedia);
        
            try {
                await modelmedia.delete(idmedia);
                res.redirect("/admin/media/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (error) {
                console.error('Error al eliminar el media:', error);
                res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
    },
};

module.exports = mediacontroller;
