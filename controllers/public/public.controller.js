const { getAll } = require("../../db/conexion");

const PublicControllers = {
    renderIndex: async (req, res) => {
        const rows = await getAll("select * from integrantes");
        console.log(rows);
        console.log(req.session);
        res.render("admin/index", {
            integrantes: rows,
            isHome: true,
            logueado: req.session.userInfo?.logueado
        });
    },

    renderCurso: async (req, res) => {
        const rows = await getAll("select * from integrantes");
        console.log(rows);
        res.render("curso", {
            integrantes: rows,
        });
    },

    renderWordCloud: async (req, res) => {
        const rows = await getAll("select * from integrantes");
        console.log(rows);
        res.render("word_cloud", {
            integrantes: rows,
        });
    },

    getIntegranteByMatricula: async (req, res, next) => {
        const matriculas = (await getAll("select matricula from integrantes where estado = 1 ")).map(obj => obj.matricula);
        const media = await getAll("select * from media where estado = 1 ");
        const matricula = req.params.matricula;
        const integrante = await getAll("select * from integrantes");

        if (matriculas.includes(matricula)) {
            const integranteFilter = await getAll("select * from integrantes where matricula = ?", [matricula]);
            const media2Filter = await getAll("select * from media2 where matricula = ?", [matricula]);
            res.render('integrantes', {
                integrantes: integranteFilter,
                media: media,
                media2: media2Filter,
                integrante: integrante,
            });
        } else {            console.log("error");
        }
    },
};

module.exports = PublicControllers;
