const {db, getAll} = require("../db/conexion");

const modelIntegrante =  {
    getAll: async function (req, res) {
        console.log("filtrado", req.query);
        let sql = "select * from integrantes Where estado = 1";
        for(const prop in req.query['s']){
            if (req.query ['s'][prop]){
                console.log("prop", prop,req.query['s'][prop]);
                sql +=` AND ${prop}='${req.query['s'][prop]}'`;
            }
        } 
        return new Promise((resolve, reject) => {
            db.all(sql, (error, integrantes) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrantes);
                }
            });
        });
    
    },
    getById : function(idIntegrante) {
        return new Promise ((resolve, reject) => {
          try{
            getAll('SELECT * FROM integrantes WHERE id = ?', [idIntegrante])
            .catch((err)=>{
              reject(err);
            })
            .then((integrantes)=>{
              resolve(integrantes);
            })
          } catch (err) {
            reject(err);
          }
        })
      },
    
    async create(req, res) {
        return new Promise((resolve, reject) => {
            db.run(
                "INSERT INTO integrantes (matricula, nombre, apellido, estado) VALUES (?, ?, ?,?)",
                [req.matricula, 
                    req.nombre, 
                    req.apellido, 
                    req.estado
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },

    update(req, idintegrante) {
        return new Promise((resolve, reject) => {
            db.run(`UPDATE integrantes SET nombre = ?, apellido = ?, matricula = ? WHERE id = ?`,
                [
                    req.nombre,
                    req.apellido,
                    req.matricula,               
                    idintegrante
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },

    delete : function(idIntegrante) {
        return new Promise ((resolve, reject) => {
          try{
            db.run(
              "UPDATE integrantes SET estado = 0 WHERE id = ?",
              [idIntegrante],
              (err) => {
                if(err) reject(err)
                else resolve();
              }
            );
          } catch (err) {
            reject(err);
          }
        })
      },
    
    



    getByField: function(tabla, clave, valor) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT 1 FROM ${tabla} WHERE ${clave} = ?`, [valor], (error, row) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(!!row);
                }
            });
        });
    },
    getAllIntegrantes: function() {
        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM integrantes WHERE activo = 1", (error, integrantes) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrantes);
                }
            });
        });
    },

    getAlltMatriculas: function() {
        return new Promise((resolve, reject) => {
            db.all("select matricula from integrantes where activo = 1 order by orden", (error, matriculas) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(matriculas);
                }
            });
        });
    },

    getByMatricula: function(matricula) {
        return new Promise((resolve, reject) => {
            db.all(`select * from integrantes where activo = 1 and matricula = ? order by orden`, [matricula], (error, integrante) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrante);
                }
            });
        });
    }




};
module.exports = modelIntegrante;
