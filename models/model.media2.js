const {db, getAll} = require("../db/conexion");

const modelmedia2 =  {
    getAll: async function (req, res) {
        console.log("filtrado", req.query);
        let sql = "select * from media2 Where estado = 1";
        for(const prop in req.query['s']){
            if (req.query ['s'][prop]){
                console.log("prop", prop,req.query['s'][prop]);
                sql +=` AND ${prop}='${req.query['s'][prop]}'`;
            }
        } 
        return new Promise((resolve, reject) => {
            db.all(sql, (error, media2) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media2);
                }
            });
        });
    
    },
    getById : function(idmedia2) {
        return new Promise ((resolve, reject) => {
          try{
            getAll('SELECT * FROM media2 WHERE id = ?', [idmedia2])
            .catch((err)=>{
              reject(err);
            })
            .then((media2)=>{
              resolve(media2);
            })
          } catch (err) {
            reject(err);
          }
        })
      },
    
      create: async function(req, res) {
        return new Promise((resolve, reject) => {
           db.run(
            "INSERT INTO media2 (Tmedia, Titulo, src, url, matricula, integrante, estado) VALUES (?, ?, ?, ?, ?, ?, ?)",
            [req.Tmedia,
                req.Titulo,
                req.src,
                req.url, 
              
            req.matricula, 
            req.integrante, 
            req.estado
            ], (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
    });

    },

    update(req, id, src) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE media2 SET Tmedia = ?, Titulo = ?, src = ?, url = ?, matricula = ?, integrante = ? WHERE id = ?",
                [
                    req.body.Tmedia,
                    req.body.Titulo,
                    src,
                    req.body.url, 
                  
                req.body.matricula, 
                req.body.integrante, 
                idmedia2
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },


    delete : function(idmedia2) {
        return new Promise ((resolve, reject) => {
          try{
            db.run(
              "UPDATE media2 SET estado = 0 WHERE id = ?",
              [idmedia2],
              (err) => {
                if(err) reject(err)
                else resolve();
              }
            );
          } catch (err) {
            reject(err);
          }
        })
      },
    
    



    getByField: function(tabla, clave, valor) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT 1 FROM ${tabla} WHERE ${clave} = ?`, [valor], (error, row) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(!!row);
                }
            });
        });
    },
    getAllmedia2: function() {
        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM media2 WHERE activo = 1", (error, media2) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media2);
                }
            });
        });
    },

    getAlltMatriculas: function() {
        return new Promise((resolve, reject) => {
            db.all("select matricula from media2 where activo = 1 order by orden", (error, matriculas) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(matriculas);
                }
            });
        });
    },

    getByMatricula: function(matricula) {
        return new Promise((resolve, reject) => {
            db.all(`select * from media2 where activo = 1 and matricula = ? order by orden`, [matricula], (error, media2) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media2);
                }
            });
        });
    }




};
module.exports = modelmedia2;