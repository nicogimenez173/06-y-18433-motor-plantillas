const { getAll, db } = require('../db/conexion');
const bcrypt = require('bcrypt');
const saltRounds = 10;

var salt;
bcrypt.genSalt(saltRounds, function(err, generetedSalt) {
  salt = generetedSalt;
});

const UsuariosModel = {

  getById : function(idUsuario) {
    return new Promise ((resolve, reject) => {
      try{
        getAll('SELECT * FROM usuarios WHERE id = ?', [idUsuario])
        .catch((err)=>{
          reject(err);
        })
        .then((usuarios)=>{
          console.log("------getById--------");
          console.log(usuarios);
          console.log("---------------------");
          resolve(usuarios);
        })
      } catch (err) {
        reject(err);
      }
    })
  },

  getUsuarios : async function(filtros) {
      console.log("filtrado:model");

      let sql = "select * from usuarios Where 1 = 1";

      for(const columna in filtros){
        if (filtros[columna]){
            console.log("columna", filtros[columna]);
            sql +=` AND ${columna}='${filtros[columna]}'`;
        }
      }
  
      console.log("sql query", sql);
      return getAll(sql);
  },

  createUsuario : function(email, password, superadmin, idIntegrante) {
    return new Promise ((resolve, reject) => {
      try{
        bcrypt.hash(password, salt, function(err, hash) {
          if(err){
            reject(err);
          }
          db.run(
            "INSERT INTO usuarios (email, password, superadmin, integrante) VALUES (?, ?, ?,?)",
            [email, hash, superadmin, idIntegrante],
            (err) => {
              if(err) reject(err)
              else resolve();
            }
          );
        });
      } catch (err) {
        reject(err);
      }
    })
  },

  updateUsuario : function(idUsuario, superadmin) {
    return new Promise ((resolve, reject) => {
      try{
        db.run(
          "UPDATE usuarios SET superadmin = ? WHERE id = ?",
          [superadmin, idUsuario],
          (err) => {
            if(err) reject(err)
            else resolve();
          }
        );
      } catch (err) {
        reject(err);
      }
    })
  },

  changePassword : function(idUsuario, password) {
    return new Promise ((resolve, reject) => {
      try{
        bcrypt.hash(password, salt, function(err, hash) {
          if(err){
            reject(err);
          }
          db.run(
            "UPDATE usuarios SET password = ? WHERE id = ?",
            [hash, idUsuario],
            (err) => {
              if(err) reject(err)
              else resolve();
            }
          );
        });
      } catch (err) {
        reject(err);
      }
    })
  }
}

module.exports =  UsuariosModel;