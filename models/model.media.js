const {db, getAll} = require("../db/conexion");

const modelmedia =  {
    getAll: async function (req, res) {
        console.log("filtrado", req.query);
        let sql = "select * from media Where estado = 1";
        for(const prop in req.query['s']){
            if (req.query ['s'][prop]){
                console.log("prop", prop,req.query['s'][prop]);
                sql +=` AND ${prop}='${req.query['s'][prop]}'`;
            }
        } 
        return new Promise((resolve, reject) => {
            db.all(sql, (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    
    },
    getById : function(idmedia) {
        return new Promise ((resolve, reject) => {
          try{
            getAll('SELECT * FROM media WHERE id = ?', [idmedia])
            .catch((err)=>{
              reject(err);
            })
            .then((media)=>{
              resolve(media);
            })
          } catch (err) {
            reject(err);
          }
        })
      },
    
    async create(req, res) {
        return new Promise((resolve, reject) => {
            db.run(
                "INSERT INTO media (nombre, estado) VALUES (?, ?)",
                [req.nombre, 
                    req.estado
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },

    update(req, idmedia) {
        return new Promise((resolve, reject) => {
            db.run(`UPDATE media SET nombre = ? WHERE id = ?`,
                [
                    req.nombre,               
                    idmedia
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },

    delete : function(idmedia) {
        return new Promise ((resolve, reject) => {
          try{
            db.run(
              "UPDATE media SET estado = 0 WHERE id = ?",
              [idmedia],
              (err) => {
                if(err) reject(err)
                else resolve();
              }
            );
          } catch (err) {
            reject(err);
          }
        })
      },
    
    



    getByField: function(tabla, clave, valor) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT 1 FROM ${tabla} WHERE ${clave} = ?`, [valor], (error, row) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(!!row);
                }
            });
        });
    },
    getAllmedia: function() {
        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM media WHERE activo = 1", (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    }




};

module.exports = modelmedia;