const {db} = require('../db/conexion');

const AuthModel = {
    getUserByEmail:  (email) => {
        return new Promise((resolve, reject) => {
            db.get('SELECT * FROM usuarios WHERE email = ?', [email], (err, user) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(user);
                }
            });
        });
    },
}
module.exports = AuthModel;
